# NF17 TD D4 - Projet Bibliothèque 
Ce dépôt contient le projet bibliothèque du groupe 2 du TD D4 (jeudi 16h30-19h30) et comprend pour l'instant :
* ce README;
* une NDC;
* un modèle logique;
* un modèle UML au format plantuml;
* un fichier de code PSQL permettant de créer les tables;
* un jeu de test.


## Membres du groupe
* Manon BEVIERE
* Quoc-Hung TRAN
* Nawid ZAFAR
* Lucien ZHANG


## Documentation

Utilisez ces tables/vues pour accéder aux données.

### Ressources
* **vLivre** : retourne la liste des livres
* **vOeuvreMusical** : retourne la liste des oeuvres musicales
* **vFilm** : retourne la liste des films
* **vExemplaire_Ressource** : retourne le nombre d'exemplaires pour chaque ressources

### Contributeurs
* **Auteur** : retourne la liste des auteurs 
* **Acteur** : retourne la liste des acteurs 
* **Compositeur** : retourne la liste des compositeurs
* **Realisateur** : retourne la liste des réalisateurs
* **Interprete** : retourne la liste des interprètes
* **vEcrit** : retourne la liste des écrivains pour un livre donné (à utiliser avec WHERE livre = [nom du livre])
* **vCompose** : retourne la liste des compositeurs pour une oeuvre musicale donnée (à utiliser avec WHERE oeuvre_musical = [nom de l'oeuvre])
* **vInterprete** : retourne la liste des interprètes pour une oeuvre musicale donnée (à utiliser avec WHERE oeuvre_musical = [nom de l'oeuvre])
* **vJoue** : retourne la liste des acteurs pour un film donné (à utiliser avec WHERE film = [nom du film])
* **vRealisateur** : retourne la liste des réalisateurs pour un film donné (à utiliser avec WHERE film = [nom du film])

### Utilisateurs (accessible uniquement par les administrateurs et modérateurs)
* **Adherent** : retourne la liste des adhérents
* **Membre_Personnel** : retourne la liste des membres du personnel
* **Blacklist** : retourne la liste des adhérents blacklistés

### Sanctions (accessible uniquement par les administrateurs et modérateurs)
* **vRetard** : retourne la liste des sanctions pour cause de retard
* **vDeterioration** : retourne la liste des sanctions pour cause de détérioration