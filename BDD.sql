CREATE TABLE Ressource(
    code VARCHAR PRIMARY KEY,
    titre VARCHAR,
    date_app DATE,
    editeur VARCHAR,
    genre VARCHAR,
    code_classification VARCHAR,
    isbn VARCHAR UNIQUE,
    resume VARCHAR,
    langue VARCHAR,
    synopsis VARCHAR,
    longueur VARCHAR,
    type_ressource CHAR(1) CHECK(type_ressource = 'L' OR type_ressource = 'O' OR type_ressource = 'F')
);

ALTER TABLE Ressource
ADD CONSTRAINT RESSOURCE_TYPE CHECK(
(type_ressource = 'L' AND isbn IS NOT NULL AND resume IS NOT NULL AND langue IS NOT NULL AND longueur IS NULL AND synopsis IS NULL) OR 
(type_ressource = 'O' AND isbn IS NULL AND resume IS NULL AND langue IS NULL AND longueur IS NOT NULL AND synopsis IS NULL) OR 
(type_ressource = 'F' AND isbn IS NULL AND resume IS NULL AND langue IS NOT NULL AND longueur IS NOT NULL AND synopsis IS NOT NULL)
);
	

CREATE TYPE Etat AS ENUM ('neuf','bon','abime','perdu');
CREATE TABLE Exemplaire( 
	id INTEGER UNIQUE,
    code VARCHAR,
	etat Etat,
	FOREIGN KEY(code) REFERENCES Ressource(code),
	PRIMARY KEY(id, code)
);

CREATE TABLE Adherent(
	login  VARCHAR PRIMARY KEY,
    mot_de_passe VARCHAR NOT NULL ,
    nom  VARCHAR, 
    prenom  VARCHAR,
    e_mail  VARCHAR,
    etat VARCHAR CHECK ( etat = 'actuel' OR etat = 'passe' ),
    date_naiss DATE ,
    num_telephone VARCHAR,
    dansBlacklist BOOLEAN,
    CHECK (length(num_telephone)=10)
);


CREATE TABLE Pret(
	date_pret DATE,
	date_fin DATE,
    exemplaire_code VARCHAR,
    exemplaire_id INTEGER,
    adherent VARCHAR,
	FOREIGN KEY(exemplaire_code,exemplaire_id) REFERENCES Exemplaire(code,id),
    FOREIGN KEY(adherent) REFERENCES Adherent(login),
	CHECK  (date_pret < date_fin),
    PRIMARY KEY(exemplaire_code, exemplaire_id, adherent)
  
);

CREATE TABLE Sanction(
    adherent VARCHAR, 
	date_debut DATE NOT NULL,
	duree INTEGER,
	estRembourse BOOLEAN,
	type_sanction CHAR NOT NULL,
	FOREIGN KEY(adherent) REFERENCES Adherent(login),
	CHECK ((type_sanction = 'D' AND estRembourse IS NOT NULL) OR type_sanction = 'R')
);



CREATE TABLE Auteur (
	id_auteur INTEGER PRIMARY KEY, 
    nom VARCHAR, 
    prenom VARCHAR,
    date_naiss VARCHAR,
    nationalite VARCHAR  
);

CREATE TABLE Acteur (
	id_acteur INTEGER PRIMARY KEY, 
    nom VARCHAR, 
    prenom VARCHAR,
    date_naiss VARCHAR,
    nationalite VARCHAR      
);

CREATE TABLE Realisateur (
	id_real INTEGER PRIMARY KEY, 
    nom VARCHAR, 
    prenom VARCHAR,
    date_naiss VARCHAR,
    nationalite VARCHAR
);

CREATE TABLE Compositeur (
	id_compositeur INTEGER PRIMARY KEY, 
    nom VARCHAR, 
    prenom VARCHAR,
    date_naiss VARCHAR,
    nationalite VARCHAR
    );

CREATE TABLE Interprete (
	id_interprete INTEGER PRIMARY KEY, 
    nom VARCHAR, 
    prenom VARCHAR,
    date_naiss VARCHAR,
    nationalite VARCHAR
    );



CREATE TABLE Ecrit(
    id_auteur INTEGER,
    code_ressource VARCHAR,
    FOREIGN KEY(id_auteur) REFERENCES Auteur(id_auteur),
    FOREIGN KEY(code_ressource) REFERENCES Ressource(code),
    PRIMARY KEY(id_auteur,code_ressource)
);

CREATE TABLE Joue(
    id_acteur INTEGER,
    code_ressource VARCHAR,
    FOREIGN KEY(id_acteur) REFERENCES Acteur(id_acteur),
    FOREIGN KEY(code_ressource) REFERENCES Ressource(code),
    PRIMARY KEY(id_acteur,code_ressource)

);

CREATE TABLE Compose(
    id_compositeur INTEGER,
    code_ressource VARCHAR,
    FOREIGN KEY(id_compositeur) REFERENCES Compositeur(id_compositeur),
    FOREIGN KEY(code_ressource) REFERENCES Ressource(code),
    PRIMARY KEY(id_compositeur,code_ressource)
 
);



CREATE TABLE Realise(
    id_real INTEGER,
    code_ressource VARCHAR,
    FOREIGN KEY(id_real) REFERENCES Realisateur(id_real),
    FOREIGN KEY(code_ressource) REFERENCES Ressource(code),
    PRIMARY KEY(id_real,code_ressource)

);


 CREATE TABLE Execute(
    id_interprete INTEGER,
    code_ressource VARCHAR,
    FOREIGN KEY(id_interprete) REFERENCES Interprete(id_interprete),
    FOREIGN KEY(code_ressource) REFERENCES Ressource(code),
    PRIMARY KEY(id_interprete,code_ressource)

);

CREATE TABLE Membre_personnel(
	login  VARCHAR PRIMARY KEY,
    mot_de_passe VARCHAR NOT NULL,
    nom  VARCHAR, 
    prenom  VARCHAR,
    e_mail  VARCHAR,
    adresse  VARCHAR
);


/* CREATION VUES */
/*CALCUL le nombre d'Exemplaire de Ressource*/

CREATE VIEW vExemplaire_ressource AS
SELECT R.titre, COUNT(E.id) AS nb_exemplaire
FROM Exemplaire E LEFT JOIN Ressource R
ON E.code=R.code
GROUP BY R.code;


/*CALCUL le nombre de sanction de adherent*/

CREATE VIEW nbsanction AS 
SELECT A.login ,A.nom,A.prenom, COUNT(*) AS nb_sanction
FROM Adherent A LEFT JOIN Sanction S
ON A.login = S.adherent
WHERE S.adherent IS NOT NULL
GROUP BY A.login;   

/*VUE VERS type de ressources et contributeur*/


CREATE VIEW vEcrit AS
SELECT E.id_auteur, Au.nom,Au.prenom, R.titre AS Livre
FROM Auteur Au,Ecrit E, Ressource R
WHERE E.code_ressource = R.code
AND E.id_auteur=Au.id_auteur
AND R.type_ressource='L';

CREATE VIEW vCompose AS
SELECT Co.id_compositeur, C.nom,C.prenom, R.titre AS Oeuvre_musical 
FROM Compose Co,Compositeur C, Ressource R
WHERE Co.code_ressource = R.code
AND Co.id_compositeur=C.id_compositeur
AND R.type_ressource='O';


CREATE VIEW vInterprete AS
SELECT Ex.id_interprete, I.nom,I.prenom, R.titre AS Oeuvre_musical  
FROM Execute Ex, Interprete I , Ressource R
WHERE Ex.code_ressource = R.code
AND Ex.id_interprete=I.id_interprete
AND R.type_ressource='O';


CREATE VIEW vJoue AS
SELECT J.id_acteur, Au.nom,Au.prenom, R.titre AS Film
FROM Joue J,Acteur Au ,Ressource R
WHERE J.code_ressource = R.code
AND J.id_acteur=Au.id_acteur
AND R.type_ressource='F';

CREATE VIEW vRealisateur AS
SELECT Re.id_real, R1.nom,R1.prenom, R.titre AS Film
FROM Realise Re,Realisateur R1, Ressource R
WHERE Re.code_ressource = R.code
AND Re.id_real=R1.id_real
AND R.type_ressource='F';

/*VUE VERS RESSOURCE*/


CREATE VIEW vLivre AS
SELECT code, type_Ressource, titre, date_app , editeur , genre , code_classification , ISBN, resume, langue
FROM Ressource
WHERE type_ressource = 'L';

CREATE VIEW vOeuvreMusical AS
SELECT code, type_Ressource , titre, date_app , editeur , genre , code_classification , longueur
FROM Ressource
WHERE type_ressource = 'O';
CREATE VIEW vFilm AS
SELECT code, type_Ressource, titre, date_app , editeur , genre , code_classification , synopsis,  longueur, langue 
FROM Ressource 
WHERE type_ressource = 'F';

/*VUE VERS SANCTION*/

CREATE VIEW vRetard AS
SELECT adherent, date_debut , duree
FROM Sanction 
WHERE type_sanction = 'R';

CREATE VIEW vDeterioration AS
SELECT adherent, date_debut , duree, estRembourse 
FROM Sanction 
WHERE type_sanction = 'D';

CREATE VIEW Blacklist  AS
SELECT nom,prenom FROM Adherent
WHERE dansBlacklist='TRUE'; 

/* Gestion des utilisateurs */
/* 3 types d'utilisateurs : Admin, Modérateur, PUBLIC */

CREATE USER Admin;
CREATE USER Moderateur;

/* Gestion des droits en fonction des personnes*/
 
GRANT ALL PRIVILEGES ON * TO Admin;

/*L'administrateur s'occupera de créer un compte modérateur pour tous les modérateurs en suivant le schéma suivant*/

GRANT ALL PRIVILEGES ON * TO Moderateur
REVOKE ALL PRIVILEGES ON Membre_personnel TO Moderateur
GRANT SELECT ON Membre_personnel TO Moderateur
 
GRANT SELECT ON Ressource, Exemplaire, Auteur, Acteur, Realisateur, Compositeur, Interprete, Ecrit, Joue, Compose, Realise, Execute TO PUBLIC;
