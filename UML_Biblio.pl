@startuml
title Relationships -Biblio

class Ressource {
#code : int {key}
titre: string
date_parution: date
editeur: string
genre: string
code_classification: string
}
class Livre {
ISBN:string
resume:string
langue:string
}
class OeuvreMusicale {
longeur:string
}

class Film {
synopsis:string
longeur:string
langue:string
}

class Contributeur {
#ID : int {key}
nom:string
prenom:string
date_naiss:string
nationalite:string
}
class Auteur{}
class Compositeur{}
class Interprete{}
class Realisateur{}
class Acteur{}

class Personne{
#login : string {key}
mot_de_passe : string{not null}
nom:string
prenom:string
email:string
adresse:string
}

class Membre_personnel{
}

class Adherent{
etat : {actuel,passe}
date_naiss: date
num_telephone: string
dansBlacklist: boolean
}


class Pret{
date_prett: date
date_fin:int
}

class Exemplaire{
etat:{neuf,bon,abime,perdu}
}

class Sanction{
date_debut: date
duree:int
}

class Retard{}

class Deterioration{
etreRembourse:boolean
}


    
   Adherent "1..*" -- "0..*" Exemplaire
   (Adherent,Exemplaire) .. Pret  	 

	Ressource <|-- Film
	Ressource <|-- Livre
	Ressource <|-- OeuvreMusicale
	Auteur --|> Contributeur
	Compositeur --|> Contributeur
	Interprete --|> Contributeur
	Realisateur --|> Contributeur
	Acteur --|> Contributeur
	Personne <|-- Membre_personnel
	Personne <|-- Adherent
	Sanction <|--  Retard
	Sanction <|-- Deterioration
    
	Livre "1.N"--"1.N"   Auteur : ecrire
	OeuvreMusicale "1.N"--"1..N"  Compositeur : compose
	OeuvreMusicale "1.N"--"1..N"   Interprete : interpréter
	Film "1.N"--"1..N"   Realisateur : realiser
	Film "1.N"--"1..N"   Acteur : jouer
	Ressource *--"1..N" Exemplaire : etre disponible de
	Adherent  *---"0.N" Sanction
@enduml

