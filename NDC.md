# Projet : base de donnée pour une bibliothèque

## Contexte
Une bibliothèque a fait appel à nous pour que l’on mette en place une base de données permettant la gestion de leurs ressources et de leurs membres. Elle doit permettre d’obtenir les informations d’une ressource quelle qu’en soit sa nature et des membres qu’ils soient du personnel ou adhérents. Elle doit assurer la gestion de la location des ressources par les membres adhérents. Enfin elle souhaite garder une trace de ses anciens adhérents.


## Objectifs
Nous allons concevoir une base de données afin de permettre le stockage et l’organisation des informations que souhaite gérer la bibliothèque. Nous ferons aussi les commandes SQL afin de faire les principales interrogations que pourraient faire les utilisateurs de la base de données et les fonctions d’ajouts et de suppressions d’éléments.

## Détails et hypothèses

### Ressource
La bibliothèque souhaite l’implémentations de ses ressources. On a choisi de diviser ces dernières en 3 grandes catégories car elles permettent de bien résumer l’intégralité des ressources avec une bonne précision :
* Livres
* EnregistrementsMusicaux
* Films 

Nous décidons de mettre ces classes en héritage de **Ressource** qui sera vue comme une classe abstraite dans la mesure où une ressource est un élément qui n’a de sens qu’avec les classes héritées.

### Exemplaires
Dans la mesure où un exemplaire dépend directement de la présence des ressources pour qu’elle soit présente, on décide de mettre cette classe en tant que composante de Ressource ; en effet, il n’y a aucun exemplaire sans ressource à exposer.

### Personne
Cette classe correspond aux informations des personnes qui ont un rapport avec les services de la bibliothèque. Ces personnes sont soit des adhérents soit des membres du personnel, ce qui nous permet de faire deux classes en héritage :
* Adhérent
* MembrePersonnel

### Contributeurs
On sépare les personnes qui effectuent des actions avec la bibliothèque des contributeurs qui eux travaillent sur différentes ressources. On décide de créer une classe abstraite dédiées à ces personnes. On va ensuite faire hériter les différents types de contributeurs qui interviennent dans les différents types de ressources.  On aura donc :
* Auteur
* Compositeur
* Interprète
* Réalisateur
* Acteur

On fait le choix de ces contributeurs car ils constituent les acteurs principaux des ressources auxquels elles sont associées. Elles seront des composantes des ressources qui leurs sont associés (auteur pour livres, compositeur et interprète pour enregistrementsMusicaux et réalisateur et acteur pour films)

### Contraintes
On fait une liaison entre Adhérent et Exemplaires car les adhérents louent des exemplaires. On ajoute les détails de la location de l’exemplaire avec une classe d’association.

## Liste des associations

**Personne**
* login{key}
* mot\_de\_passe{not null}
* nom
* prenom
* email
* adresse
___

**Adherent**
* etat : {actuel, passé}
* date\_naissance{not null}
* numero\_tel
* dansBlacklist: boolean 

Hérite de Personne

Doit s'identifier pour emprunter un exemplaire

___

**MembrePersonnel**

Hérite de Personne
___

**Ressource**
* code{key}
* titre
* date\_parution
* editeur
* genre
* code\_classification

Une ressource est composée d'Exemplaires
___

**Exemplaire**
* ID{key}
* etat : {neuf, bon, abîmé, perdu}

ID est une clé artificielle. On fait le choix de mettre une telle clé dans la mesure où aucun des attributs d'Exemplaire ne peut avoir ce rôle

___

**Livre**
* isbn
* resume
* langue

Hérite de Ressource

A un ou plusieurs Auteurs
___

**Film**
* langue
* longueur
* synopsis

Hérite de Ressource

A un ou plusieurs Réalisateurs

A un ou plusieurs Acteurs
___

**OeuvreMusicale**
* longueur

Hérite de Ressource

A un ou plusieurs Compositeurs

A un ou plusieurs Interprêtes
___

**Contributeur**
* nom
* prenom
* date_naissance
* nationalite
___

**Auteurs**

Hérite de Contributeur

Peut contribuer à plusieurs livres
___

**Compositeur**

Hérite de Contributeur

Peut contribuer à plusieurs oeuvres musicales
___

**Interprète**

Hérite de Contributeur

Peut contribuer à plusieurs oeuvres musicales
___

**Acteur**

Hérite de Contributeur

Peut contribuer à plusieurs films
___

**Réalisateur**

Hérite de Contributeur

Peut contribuer à plusieurs films
___

**Prêt**
* adhérent{key} (référence à Adhérent)
* date_pret{not null}
* duree{not null}
* exemplaire{key} {référence à Exemplaire}

Un prêt ne concerne qu'un seule adhérent et qu'un seul exemplaire

Un adhérent n'a le droit d'emprunter qu'un nombre limité de livres à la fois

L'exemplaire emprunté doit être disponible (pas déjà emprunté) et en bon état (etat = neuf ou bon)

___ 

**Sanction**
* adherent {key}{référence à Prêt}
* exemplaire {key} {référence à prêt }
* date_debut
* duree

il y a deux type de sanctions

___

**Retard**

Hérite de Sanction

Dans le cas Retard dans la restitution des documents empruntés entraîne une suspension du droit de prêt d'une durée égale au nombre de jours de retard.

___

**Deterioration**

Hérite de Sanction

Dans le cas perte ou détérioration grave d'un document, la suspension du droit de prêt est maintenue jusqu'à ce que l'adhérent rembourse le document.

* etreRembourse:boolean 




## Utilisateurs

* Administrateur : accès à tout
* Membres du personnel : accès à tout sauf aux données concernant les membres du personnel
* Adhérents : accès aux ressources et aux contributeurs

## Fonctions à implémenter 

* Suspension du droit de prêt d’un adhérent 
* Blacklister un adhérent 
* Authentification 
* Emprunter un document
* Calcul de statistiques sur les documents empruntés par les adhérents
