/*AJOUTER des DONNEES*/

INSERT INTO Adherent (login,mot_de_passe,nom,prenom,e_mail, etat, date_naiss, num_telephone, dansBlacklist) VALUES ('traquoch', '1234', 'TRAN', 'Qh','qht@gmail.com','actuel', '1999-09-02', '0635480070', False);
INSERT INTO Adherent (login,mot_de_passe,nom,prenom,e_mail, etat, date_naiss, num_telephone, dansBlacklist) VALUES ('pauli', '12344', 'PAUL', 'POGBA','paul_Po@gmail.com','actuel', '1999-04-02', '0634480078', False);
INSERT INTO Adherent (login,mot_de_passe,nom,prenom,e_mail, etat, date_naiss, num_telephone, dansBlacklist) VALUES ('lephuock', '1134', 'LE', 'PK','lpk@gmail.com','actuel', '1998-09-02', '0455480070', TRUE);
INSERT INTO Adherent (login,mot_de_passe,nom,prenom,e_mail, etat, date_naiss, num_telephone, dansBlacklist) VALUES ('messi', '1234', 'LIONEL', 'messi','lion@gmail.com','passe', '1499-09-02', '0635580070', False);
INSERT INTO Adherent (login,mot_de_passe,nom,prenom,e_mail, etat, date_naiss, num_telephone, dansBlacklist) VALUES ('manonb', 'AZERTY', 'BEVIERE', 'Manon','manon@gmail.com','actuel', '1999-10-04', '0606060606', False);
INSERT INTO Adherent (login,mot_de_passe,nom,prenom,e_mail, etat, date_naiss, num_telephone, dansBlacklist) VALUES ('robert', '3567', 'ROBERT', 'KAY','robertk@gmail.com','actuel', '1999-10-04', '0606060606', TRUE);


INSERT INTO Membre_personnel (login,mot_de_passe,nom,prenom,e_mail,adresse) VALUES ('uyen', '1234', 'TRAN', 'uyen','trauyen@gmail.com','Chine');
INSERT INTO Membre_personnel (login,mot_de_passe,nom,prenom,e_mail,adresse) VALUES ('DUP', '123as4', 'DUPOINT', 'A','dp@gmail.com','France');

INSERT INTO Ressource(Code, Titre, date_app,editeur,genre, code_classification,ISBN, resume, langue, synopsis, longueur,type_ressource) VALUES ('5','Les Misérables','1862-10-06','S','Roman', 10,'8ZAU2832B','miserables','Français',NULL,NULL,'L');
INSERT INTO Ressource(Code, Titre, date_app,editeur,genre, code_classification,ISBN, resume, langue, synopsis, longueur,type_ressource) VALUES ('7','Cogito','2016-10-06','S','Roman', 11,'DHI3H823','cogito','Français',NULL,NULL,'L');
INSERT INTO Ressource(Code, Titre, date_app,editeur,genre, code_classification,ISBN, resume, langue, synopsis, longueur,type_ressource) VALUES ('8','Phobos','2017-12-06','S','Roman', 12,'JFF9RU39','phobos','Français',NULL,NULL,'L');
INSERT INTO Ressource(Code, Titre, date_app,editeur,genre, code_classification,ISBN, resume, langue, synopsis, longueur,type_ressource) VALUES ('9','Avengers : Endgame','2019-04-24','S', 'sience_fiction ', 11,NULL,NULL,'anglais','Thanos','181 minutes','F');
INSERT INTO Ressource(Code, Titre, date_app,editeur,genre, code_classification,ISBN, resume, langue, synopsis, longueur,type_ressource) VALUES ('1','Sonata_piano','1900-04-24','S', 'music_classical ', 20,NULL,NULL,NULL,NULL,'10 minutes','O');

INSERT INTO Exemplaire(id,code,etat) VALUES (1,'5','neuf');
INSERT INTO Exemplaire(id,code,etat) VALUES (2,'5','bon');
INSERT INTO Exemplaire(id,code,etat) VALUES (3,'5','bon');
INSERT INTO Exemplaire(id,code,etat) VALUES (4,'7','abime');
INSERT INTO Exemplaire(id,code,etat) VALUES (5,'8','neuf');
INSERT INTO Exemplaire(id,code,etat) VALUES (6,'9','neuf');
INSERT INTO Exemplaire(id,code,etat) VALUES (7,'7','bon');
INSERT INTO Exemplaire(id,code,etat) VALUES (8,'8','perdu');
INSERT INTO Exemplaire(id,code,etat) VALUES (9,'1','neuf');
INSERT INTO Exemplaire(id,code,etat) VALUES (10,'1','perdu');

INSERT INTO Auteur(id_auteur,nom,prenom,date_naiss,nationalite) VALUES (1,'Hugo','Victor','1802-02-26','Français');
INSERT INTO Auteur(id_auteur,nom,prenom,date_naiss,nationalite) VALUES (2,'Zola','Emile','1840-04-02','Français');
INSERT INTO Auteur(id_auteur,nom,prenom,date_naiss,nationalite) VALUES (3,'Dixen','Victor','1949-11-08','Français');

INSERT INTO Ecrit(id_auteur,code_ressource) VALUES (1,'5');
INSERT INTO Ecrit(id_auteur,code_ressource) VALUES (3,'7');
INSERT INTO Ecrit(id_auteur,code_ressource) VALUES (3,'8');

INSERT INTO Acteur(id_acteur,nom,prenom,date_naiss,nationalite) VALUES (1,'Chris','Evans','1981-06-13','americain');
INSERT INTO Acteur(id_acteur,nom,prenom,date_naiss,nationalite) VALUES (2,'Tony','Starck','1965-04-04','americain');
INSERT INTO Acteur(id_acteur,nom,prenom,date_naiss,nationalite) VALUES (3,'Mark','Ruffano','1949-11-09','american');

INSERT INTO Joue(id_acteur,code_ressource) VALUES (1,'9');
INSERT INTO Joue(id_acteur,code_ressource) VALUES (2,'9');
INSERT INTO Joue(id_acteur,code_ressource) VALUES (3,'9');

INSERT INTO Realisateur(id_real,nom,prenom,date_naiss,nationalite) VALUES (1,'Joe','Russo','1920-02-02','American');
INSERT INTO Realisateur(id_real,nom,prenom,date_naiss,nationalite) VALUES (2,'Russo','Anthony','1970-02-03','Américain');
INSERT INTO Realise(id_real, code_ressource) VALUES (1,9);
INSERT INTO Realise(id_real,code_ressource) VALUES (2,'9');

INSERT INTO Compositeur(id_compositeur,nom,prenom,date_naiss,nationalite) VALUES (1,'Mozart','Wolgang','1756-01-27','Autriche');
INSERT INTO Compose(id_compositeur,code_ressource) VALUES (1,1);

INSERT INTO Interprete(id_interprete,nom,prenom,date_naiss,nationalite) VALUES (1,'David','Peter','1799-01-27','Autriche');
INSERT INTO Execute(id_interprete, code_ressource) VALUES (1,1);

INSERT INTO Pret(date_pret,date_fin,exemplaire_code,exemplaire_id,adherent) VALUES ('2020-02-07','2020-02-14','5','1','manonb');
INSERT INTO Pret(date_pret,date_fin,exemplaire_code,exemplaire_id,adherent) VALUES ('2020-01-13','2020-01-26','9','6','traquoch');
INSERT INTO Pret(date_pret,date_fin,exemplaire_code,exemplaire_id,adherent) VALUES ('2019-07-12','2020-08-12','7','4','messi');

INSERT INTO Sanction(adherent,date_debut,duree,estRembourse,type_sanction) VALUES ('manonb','2020-04-01',12,False,'D');
INSERT INTO Sanction(adherent,date_debut,duree,estRembourse,type_sanction) VALUES ('manonb','2019-06-07',12,True,'D');
INSERT INTO Sanction(adherent,date_debut,duree,estRembourse,type_sanction) VALUES ('pauli','2017-03-08',12,NULL,'R');
